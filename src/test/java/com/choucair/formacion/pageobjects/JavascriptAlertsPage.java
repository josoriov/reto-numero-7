package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://the-internet.herokuapp.com/")
public class JavascriptAlertsPage extends PageObject {

	//Link JavaScript Alerts
	@FindBy(xpath="//*[@id=\'content\']/ul/li[25]/a")
	public WebElement JavaScriptAlert;
	
	//Boton Js Alert
	@FindBy(xpath="//*[@id='content']/div/ul/li[1]/button")
	public WebElement btnJsAlert;
	
	//Boton Js Confirm
	@FindBy(xpath="//*[@id=\'content\']/div/ul/li[2]/button")
	public WebElement btnJsConfirm;
	
	//Boton Js Prompt
	@FindBy(xpath="//*[@id=\'content\']/div/ul/li[3]/button")
	public WebElement btnJsPrompt;
	
	//Mensaje de js Prompt
	@FindBy(id="result")
	public WebElement txtmensaje;
	
	public void javascriptAlertClick() {
		JavaScriptAlert.click();
	}
	
	public void jsAlertClick() {
		btnJsAlert.click(); 
		
	}
	public void jsPromptClick() {
		btnJsPrompt.click();
	}
	public void jsConfirmClick() {
		btnJsConfirm.click();
	}

	public void validarMensaje() {
		String strMensaje = txtmensaje.getText();
		assertThat(strMensaje, containsString("hola mundo"));
	}
}
