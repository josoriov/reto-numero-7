package com.choucair.formacion.steps;

import org.fluentlenium.core.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.context.support.StaticApplicationContext;

import com.choucair.formacion.pageobjects.JavascriptAlertsPage;

import net.thucydides.core.annotations.Step;

public class JavascriptAlertsSteps {

	
	JavascriptAlertsPage javascriptAlertsPage;
	
	@Step
	public void ingresar_herokuapp() {
		javascriptAlertsPage.open();
		javascriptAlertsPage.javascriptAlertClick();
		
	}

	public void jsAlert() {
		javascriptAlertsPage.jsAlertClick();
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.getMessage();
		}
		javascriptAlertsPage.getDriver().switchTo().alert().accept();
	}

	public void jsConfirm() {
		javascriptAlertsPage.jsConfirmClick();
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.getMessage();
		}
		javascriptAlertsPage.getDriver().switchTo().alert().dismiss();
		
	}

	public void jsPrompt() {
		javascriptAlertsPage.jsPromptClick();
		javascriptAlertsPage.getDriver().switchTo().alert().sendKeys("hola mundo");
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.getMessage();
		}
		javascriptAlertsPage.getDriver().switchTo().alert().accept();
		
	}

	public void validarMensajeJsPrompt() {
		javascriptAlertsPage.validarMensaje();		
	}

}
