package com.choucair.formacion.definition;

import com.choucair.formacion.steps.JavascriptAlertsSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class JavascriptAlertsDefinition {

	@Steps
	JavascriptAlertsSteps javascriptAlertsSteps;

	
	@Given("^ingresar a la página de HeroKuapp y selecciona el link javascript_alerts$")
	public void ingresar_a_la_página_de_HeroKuapp_y_selecciona_el_link_javascript_alerts()  {
	    // Write code here that turns the phrase above into concrete actions
		javascriptAlertsSteps.ingresar_herokuapp();
		
	}

	@When("^interactúa con los controles que hay en esta pantalla$")
	public void interactúa_con_los_controles_que_hay_en_esta_pantalla() throws Throwable {
		
		
		javascriptAlertsSteps.jsAlert();
		javascriptAlertsSteps.jsConfirm();
		javascriptAlertsSteps.jsPrompt();
	}

	@When("^validar mensaje del  Alert Prompt$")
	public void validar_mensaje_del_Alert_Prompt() throws Throwable {
	 javascriptAlertsSteps.validarMensajeJsPrompt();
	}
}
